package com.nikadmin.smartpillbox;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class DBController extends SQLiteOpenHelper {

    private static final int Ver = 1;

    private static final String DB_NAME = "PillBoxLogs";
    private static final String TABLE_LOGS = "logs";
    private static final String FIELD_DATETIME = "datetime";
    private static final String FIELD_MESSAGE = "message";

    static class LogLine {
        long datetime;
        String message;

        LogLine(long dt, String msg) {
            datetime = dt;
            message = msg;
        }
    }

    DBController(Context context) {
        super(context, DB_NAME, null, Ver);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        // CREATE TABLE logs (
        //      datetime INTEGER NOT NULL,
        //      message TEXT NOT NULL);

        db.execSQL("CREATE TABLE " + TABLE_LOGS + "( "
                        + FIELD_DATETIME + " INTEGER NOT NULL, "
                        + FIELD_MESSAGE + " TEXT NOT NULL);"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    void log(String message) {
        SQLiteDatabase db = getWritableDatabase();

        // INSERT INTO logs (datetime, message) VALUES (CurrentTime, message);

        ContentValues c = new ContentValues();
        c.put(FIELD_DATETIME, System.currentTimeMillis() / 1000);
        c.put(FIELD_MESSAGE, message);

        db.insert(TABLE_LOGS, null, c);
    }

    List<LogLine> getLastLogs(int count) {
        List<LogLine> logs = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();

        //the best way to get last % records is to order records in reverse and take first % records

        // SELECT datetime, message FROM logs ORDER BY datetime DESC LIMIT count;

        Cursor c = db.query(TABLE_LOGS, new String[] {FIELD_DATETIME, FIELD_MESSAGE},
                null, null, null, null,
                FIELD_DATETIME + " DESC", Integer.toString(count));

        c.moveToFirst();


        while (!c.isAfterLast()) {
            long datetime = c.getLong(c.getColumnIndex(FIELD_DATETIME));
            String message = c.getString(c.getColumnIndex(FIELD_MESSAGE));

            //since records are in reverse order, inserting them at the list start will put them in normal order
            logs.add(0, new LogLine(datetime, message));

            c.moveToNext();
        }

        c.close();

        return logs;
    }

    void clear() {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_LOGS + ";");
    }
}
