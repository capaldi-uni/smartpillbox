package com.nikadmin.smartpillbox;

import android.content.Context;
import android.util.Log;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.io.UnsupportedEncodingException;

class MQTTController {

    public interface ControllerCallback {
        void onMessage(String message);
    }

    private static final String TAG = "MQTTController";


    //singleton
    private static MQTTController instance = null;

    static MQTTController getInstance() {

        if (instance == null) {     //lazy initialization
            instance = new MQTTController();
        }

        return instance;
    }

    private MQTTController() { }


    //object members

    private ControllerCallback mainCallback;

    private Context appContext;

    private String clientId;
    private MqttAndroidClient client;


    void init(ControllerCallback callback, Context context) {

        appContext = context;   //MqttAndroidClient requires app context to access Mqtt service, permissions and so on
        mainCallback = callback;

        clientId = MqttClient.generateClientId();

        establishConnection();
    }

    private void establishConnection() {

        try {
            //broker address is retrieved from string resources, but can easily be passed as argument to init method
            client = new MqttAndroidClient(appContext, appContext.getString(R.string.server), clientId);

            client.setCallback(new MqttCallback() {
                @Override
                public void connectionLost(Throwable throwable) {
                    Log.d(TAG, "Connection lost");
                }

                @Override
                public void messageArrived(String s, MqttMessage mqttMessage) throws Exception {
                    Log.d(TAG, "New message: " + mqttMessage.toString());
                    mainCallback.onMessage(mqttMessage.toString()); //call callback with received message
                }

                @Override
                public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
                    Log.d(TAG, "Delivery complete");
                }
            });

            final IMqttToken token = client.connect();

            token.setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken iMqttToken) {
                    Log.d(TAG, "Connected successfully");

                    try {
                        //topic also can be passed as parameter or set other way
                        IMqttToken topictoken = client.subscribe(appContext.getString(R.string.topic), 0);

                        topictoken.setActionCallback(new IMqttActionListener() {
                            @Override
                            public void onSuccess(IMqttToken iMqttToken) {
                                Log.d(TAG, "Subscribed successfully");
                            }

                            @Override
                            public void onFailure(IMqttToken iMqttToken, Throwable throwable) {
                                Log.d(TAG, "Subscription failed");
                                Log.d(TAG, "Exception: " + throwable.toString());
                            }
                        });

                    } catch (MqttException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(IMqttToken iMqttToken, Throwable throwable) {
                    Log.d(TAG, "Connection failed");
                    Log.d(TAG, "Exception: " + throwable.toString());
                }
            });

        } catch (MqttException e) {
            Log.d(TAG, "MQTT client exception");
            e.printStackTrace();
        }

    }

    void sendMessage(MainActivity.Message message) {
        try {
            byte[] payload = message.toString().getBytes("UTF-8");
            MqttMessage mqttMessage = new MqttMessage(payload);
            client.publish(appContext.getString(R.string.topic), mqttMessage);
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            Log.d(TAG,"Unsupported encoding");
        }
        catch (MqttException e) {
            e.printStackTrace();
            Log.d(TAG,"Exception while publishing message: " + e.toString());
        }
    }
}
